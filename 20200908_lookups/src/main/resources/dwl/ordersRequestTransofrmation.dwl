%dw 1.0
%output application/json skipNullOn="everywhere"
---
{
	customerId:payload.id as :number default null,
	category:payload.category default "",
	netprice: payload.price.netPrice default 0,
	discountedprice: payload.price.discountedprice,
	taxPercentage: p('dw.taxPercentage')
}