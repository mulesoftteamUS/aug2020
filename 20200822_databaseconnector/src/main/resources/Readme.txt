company Table
create table company(comapnyId varchar(100) not null,companyName varchar(200),companyAddress varchar(1000))

4 APIs to be built
	1) Insert data - POST
		post: localhost:8081/api/company
		REQUEST: 
		
			{
				"id":"3",
			    "name":"Bugati",
			    "address":"Germany"
			}
		RESPONSE:
		
			{
			    "message": "Record inserted succefully"
			}

	2) retrieve data - GET Completed
		GET: localhost:8081/api/company	
		RESPONSE:
			[
			  {
			    "companyAddress": "Silicon valley, US",
			    "companyName": "Amazon",
			    "comapnyId": "1"
			  },
			  {
			    "companyAddress": "Arizona, US",
			    "companyName": "Apple",
			    "comapnyId": "2"
			  },
			  {
			    "companyAddress": "Germany",
			    "companyName": "Bugati",
			    "comapnyId": "3"
			  },
			  {
			    "companyAddress": "Germany",
			    "companyName": "Bugati",
			    "comapnyId": "3"
			  }
			]

	3) DELETE: localhost:8081/api/company
	    REQUEST:
	        {
	        	"id":1
	        }
		RESPONSE:
			{
				"Message":"Company Record 2 is deleted"
			}